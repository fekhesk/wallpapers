# Wallpapers

yiffOS wallpapers repository

Packaged as [wallpapers](https://packages.yiffos.gay/wallpapers)

## Submission

Open a MR on the [GitLab project](https://gitlab.com/yiffos/misc/wallpapers) and fill out the provided template.

Ensure you have full rights to the image and you agree to license the images under CC BY-NC-SA 4.0.

Minimum Size:
* 1920 x 1080 (16:9)

Please do not submit wallpapers that use the following formats:
* PSDs
* JPEGs (unless you are also providing source files)
* PNGs (same as above)
* OBJ
* MA / MB
* C4D
* 3D Objects (unless they are to be expoted as a flat image)
* Videos (??)
* Audio (???????????)
